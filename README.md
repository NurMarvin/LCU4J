# LCU4J
A Java library for the League of Legends Client API
* * *

LCU4J is a library written for Java, which provides an interface to the League Client API.
It uses the official APIs and executes the data via a HTTPS connection.

For the beginning we will realize the access to the following APIs:
- lol-service-status
- lol-settings
- lol patch
- lol-featured-modes
- lol-active-boosts
- lol-game-client-chat
- lol-lobby
- lol-maps
- lol-player-messaging
- lol-summoner (only the method current_summoner)

Over time, the remaining APIs will follow.
Since there is little or no documentation for the League Client APIs, there will also be documentation about our library, which may also be interesting for platforms other than Java.

We recommend to use our [wiki](https://bitbucket.org/eonadev/lcu4j/wiki/) for first usage!

This project will also participate in the competition "The Riot Games API Challenge 2018".

Authors of this library are:
- [EONA Bloodrayne](https://github.com/Bloodrayne1995 )
- [F4llindor](https://github.com/KleinDevDE)

Both are members of the EonaDEV team.


