package de.eonadev.lcu4j;


import com.google.gson.Gson;
import de.eonadev.lcu4j.enums.LCUEndpoint;
import de.eonadev.lcu4j.exceptions.EndpointNotFound;
import de.eonadev.lcu4j.exceptions.InstallationPathException;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.plugins.lolactiveboosts.LolActiveBoosts;
import de.eonadev.lcu4j.plugins.lollobby.LolLobby;
import de.eonadev.lcu4j.plugins.lolmaps.LolMaps;
import de.eonadev.lcu4j.plugins.lolservicestatus.LolServiceStatus;
import de.eonadev.lcu4j.plugins.lolsettings.LolSettings;
import de.eonadev.lcu4j.plugins.lolsummoner.LolSummoner;
import de.eonadev.lcu4j.utils.ApacheWebClient;
import de.eonadev.lcu4j.utils.ErrorManager;
import de.eonadev.lcu4j.utils.Utils;
import java.io.File;
import java.util.HashMap;

/**
 * general endpoint for this library
 */
public class LCU4J {

    public static LCU4J instance = null;

    /**
     * installation path of league of legends, cause this can differ from installations
     */
    private String riot_base_directory = "";

    /**
     * client to access the web-services
     */
    private ApacheWebClient client = null;

    /**
     * GSON-object to serialize and deserialize JSON-strings
     */
    private Gson main_gson = new Gson();

    /**
     * List of all active endpoints
     */
    private HashMap<LCUEndpoint, BasePlugin> api_endpoints = new HashMap<>();



    /**
     * Initiate a new LCU4J object to access to the endpoints
     * @param riot_base_directory
     */
    public LCU4J(String riot_base_directory) {
        this.riot_base_directory = "";
        try {
            this.setRiot_base_directory(riot_base_directory);
        } catch (InstallationPathException e) {
            ErrorManager.getInstance().raiseException(e);
            return;
        }

        client = ApacheWebClient.instance;
        Object conn_data[] = Utils.getWebClientData(getRiot_base_directory());

        client.setConnectionData((int) conn_data[0], (String) conn_data[1]);
        client.openClient();
        initEndpoints();
        instance = this;
    }

    /**
     * Initiate a new LCU4J object to access to the endpoints
     */
    public LCU4J() {
        this(Utils.getRiotBaseDirectory());
        instance = this;
    }

    /**
     * returns the installation path of league of legends
     * @return
     */
    public String getRiot_base_directory() {
        return riot_base_directory;
    }

    /**
     * set the installation path of league of legends
     * @param riot_base_directory
     */
    public void setRiot_base_directory(String riot_base_directory) throws InstallationPathException {
        //Check if path exists
        if (new File(riot_base_directory).exists())
            this.riot_base_directory = riot_base_directory;
        else
            throw new InstallationPathException("Could not find \"League of Legends\" installation directory!", riot_base_directory);
    }

    /**
     * Initialize the endpoints
     */
    private void initEndpoints() {
        try {
            api_endpoints.put(LCUEndpoint.lol_service_status, new LolServiceStatus());
            api_endpoints.put(LCUEndpoint.lol_active_boosts, new LolActiveBoosts());
            api_endpoints.put(LCUEndpoint.lol_summoner, new LolSummoner());
            api_endpoints.put(LCUEndpoint.lol_maps, new LolMaps());
            api_endpoints.put(LCUEndpoint.lol_settings,new LolSettings());
            api_endpoints.put(LCUEndpoint.lol_lobby,new LolLobby());
        } catch (Exception e) {
            ErrorManager.getInstance().raiseException(e);
        }
    }

    /**
     * returns the webclient object to execute against League API
     * @return
     */
    public ApacheWebClient getClient() {
        return client;
    }

    /**
     * returns the main gson-object to serialize and deserialize JSON-strings
     * @return
     */
    public Gson getMain_gson() {
        return main_gson;
    }


    /**
     * returns an API endpoint of a specific API
     * @return
     */
    public BasePlugin getEndpoint(LCUEndpoint api) {
        if (api_endpoints.containsKey(api)) {
            return api_endpoints.get(api);
        } else {
            ErrorManager.getInstance().raiseException(new EndpointNotFound(api.getLCUendpoint()));
            return null;
        }
    }


    /**
     * Clear data and close connections before JVM remove this instance
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        client.closeClient();
        super.finalize();
    }
}
