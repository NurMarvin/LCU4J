package de.eonadev.lcu4j.exceptions;


public class EndpointNotFound extends Exception {

    private String error_endpoint = "";

    public EndpointNotFound(String endpoint){
        super("The given endpoint is not initiated: " + endpoint);
        error_endpoint = endpoint;
    }


    public String getError_endpoint() {
        return error_endpoint;
    }
}
