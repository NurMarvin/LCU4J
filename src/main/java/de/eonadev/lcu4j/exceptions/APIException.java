package de.eonadev.lcu4j.exceptions;

import de.eonadev.lcu4j.utils.APIResponse;

public class APIException extends Exception {

    private APIResponse response = null;

    public APIException(String message, APIResponse response){
        super(message);
        this.response = response;
    }


    public APIResponse getResponse() {
        return response;
    }
}
