package de.eonadev.lcu4j.exceptions;

public class InstallationPathException extends Exception {

    private String path;

    public InstallationPathException(String message, String path) {
        super(message);
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
