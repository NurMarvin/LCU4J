package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class BadRequestException extends APIException {
    //400


    public BadRequestException(APIResponse response){
        super("Bad Request",response);
    }

}
