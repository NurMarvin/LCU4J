package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class InternalServerErrorException extends APIException {
    //500


    public InternalServerErrorException(APIResponse response) {
        super("Internal Server Error", response);
    }
}
