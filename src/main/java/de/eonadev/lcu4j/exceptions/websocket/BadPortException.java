package de.eonadev.lcu4j.exceptions.websocket;

public class BadPortException extends Exception {

    public BadPortException(int port) {
        super("Port " + port + " is not a valid port (port must be within 0 and 65535");
    }


}
