package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class ServiceUnavailableException extends APIException {
    //503


    public ServiceUnavailableException(APIResponse response) {
        super("Service Unavailable", response);
    }
}
