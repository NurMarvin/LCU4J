package de.eonadev.lcu4j.exceptions;

public class SettingNotFoundException extends Exception {

    public SettingNotFoundException(String key){
        super("Setting wurde nicht gefunden: " + key);
    }

}
