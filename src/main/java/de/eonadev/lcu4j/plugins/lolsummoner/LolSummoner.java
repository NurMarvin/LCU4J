package de.eonadev.lcu4j.plugins.lolsummoner;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.exceptions.websocket.*;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

/**
 * Endpoint for API lol-summoner
 *
 * @apiNote This endpoint has more functions than current summoner. This project is an entry of the RIOT API-Challenge 2018.
 * This limited the usage of this api to getCurrentSummoner only
 */
public class LolSummoner extends BasePlugin {

    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean is_created = false;

    public LolSummoner() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-summoner");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */


    /**
     * Retrieve the information of the current summoner
     *
     * @return
     */
    public LolSummonerSummoner getCurrentSummoner() {
        APIResponse data = getData("lol-summoner/v1/current-summoner");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolSummonerSummoner.class);
        } else {
            ErrorManager.getInstance().handleAPIError(data);
            return null;
        }
    }

}
