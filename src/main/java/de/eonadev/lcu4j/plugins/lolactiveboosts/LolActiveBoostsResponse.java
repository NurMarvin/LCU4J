package de.eonadev.lcu4j.plugins.lolactiveboosts;

/**
 * Response for lol-active-boosts
 */
public class LolActiveBoostsResponse {

    /**
     * Date when the next "First win of the Day"-Boost is enabled
     */
    private String firstWinOfTheDayStartTime = "";

    /**
     * Date when the IP boost (over time) is ending (is now BE)
     */
    private String ipBoostEndDate = "";

    /**
     * Count of how many wins the IP boost is remaining (is now BE)
     */
    private int ipBoostPerWinCount = 0;

    /**
     * Something with IP boosts (now BE). Unknown feature
     */
    private int ipLoyalityBoosts = 0;

    /**
     * ID of the logged in summoner
     */
    private int summonerID = 0;

    /**
     * Date when the time based XP-boost ends
     */
    private String xpBoostEndDate = "";

    /**
     * Count of how many wins the XP boost is remaining
     */
    private int xpBoostPerWinCount = 0;

    /**
     * Something with XP boosts (now BE). Unknown feature
     */
    private int xpLoyalityBoosts = 0;

    LolActiveBoostsResponse() {
    }

    /**
     * returns the date when the "first win of the day"-boost is enabled
     *
     * @return
     */
    public String getFirstWinOfTheDayStartTime() {
        return firstWinOfTheDayStartTime;
    }

    /**
     * returns the date when the time-based IP-boost ends
     *
     * @return
     */
    public String getIpBoostEndDate() {
        return ipBoostEndDate;
    }

    /**
     * returns the remaining count of wins for the IP-boosts
     *
     * @return
     */
    public int getIpBoostPerWinCount() {
        return ipBoostPerWinCount;
    }

    /**
     * return something with IP-boost. Unknown feature
     *
     * @return
     */
    public int getIpLoyalityBoosts() {
        return ipLoyalityBoosts;
    }

    /**
     * returns the summoner id of the current logged user
     *
     * @return
     */
    public int getSummonerID() {
        return summonerID;
    }

    /**
     * returns the date which the time-based XP-boost ends
     *
     * @return
     */
    public String getXpBoostEndDate() {
        return xpBoostEndDate;
    }

    /**
     * returns the remaining count of wins for the XP-boost
     *
     * @return
     */
    public int getXpBoostPerWinCount() {
        return xpBoostPerWinCount;
    }

    /**
     * Something with XP-boosts. Unknown Feature
     *
     * @return
     */
    public int getXpLoyalityBoosts() {
        return xpLoyalityBoosts;
    }
}
