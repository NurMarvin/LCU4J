package de.eonadev.lcu4j.plugins.loltrophies;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;

public class LolTrophies extends BasePlugin {
    /**
     * SINGLETON START (access shall only be over LCU4J)
     */

    private static boolean is_created = false;

    public LolTrophies() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-spectator");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */

    /**
     * Returns the Informations about acutally spectating game
     *
     * @return LolSpectatorSpectateGameInfo.class
     */
    public LolTrophiesTrophyProfileData getLolTrophiesTrophyProfileData() {
        APIResponse data = getData("lol-spectator/v1/spectate");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolTrophiesTrophyProfileData.class);
        } else {
            return null;
        }
    }
}
