package de.eonadev.lcu4j.plugins;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.utils.APIResponse;

public abstract class BasePlugin {


    protected APIResponse postData(String method, String json_data) {
        return LCU4J.instance.getClient().executePOST(method, json_data);
    }

    protected APIResponse getData(String method) {
        return LCU4J.instance.getClient().executeGET(method);
    }

    protected APIResponse putData(String method, String json_data){
        return LCU4J.instance.getClient().executePUT(method,json_data);
    }

    protected APIResponse patchData(String method, String json_data){
        return LCU4J.instance.getClient().executePATCH(method, json_data);
    }

    protected APIResponse deleteData(String method){
        return LCU4J.instance.getClient().executeDELETE(method);
    }

}