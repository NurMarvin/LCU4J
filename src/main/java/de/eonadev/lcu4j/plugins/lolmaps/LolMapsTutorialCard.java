package de.eonadev.lcu4j.plugins.lolmaps;

/**
 * Represents a tutorial card of a map
 */
public class LolMapsTutorialCard {

    /**
     * general content of the card
     */
    private String description = "";

    /**
     * footer of the card
     */
    private String footer = "";

    /**
     * header of the card
     */
    private String header = "";

    /**
     * path to an image which is shown in the card
     */
    private String imagePath = "";


    LolMapsTutorialCard() {
    }

    /**
     * returns the content of the card
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * returns the footer of the card
     *
     * @return
     */
    public String getFooter() {
        return footer;
    }

    /**
     * returns the header of the card
     *
     * @return
     */
    public String getHeader() {
        return header;
    }

    /**
     * returns the path to the image which is shown in the card
     *
     * @return
     */
    public String getImagePath() {
        return imagePath;
    }
}
