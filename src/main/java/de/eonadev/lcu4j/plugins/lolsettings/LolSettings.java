package de.eonadev.lcu4j.plugins.lolsettings;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.enums.EndpointVersion;
import de.eonadev.lcu4j.enums.PPType;
import de.eonadev.lcu4j.enums.SettingCategory;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

public class LolSettings extends BasePlugin {

    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean is_created = false;

    public LolSettings() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-settings");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */


    /**
     * Save the settings
     * @apiNote POST /lol-settings/v1/account/save [VERSION 1]
     */
    public void saveAccount(){
        APIResponse result = postData("lol-settings/v1/account/save",null);
        if(result.getHttpStatus() != 204){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * Save settings in Account namespace
     * @param category  Category of the setting
     * @param obj   BaseSetting to save
     * @param apply_directly true ==> will apply directly (PATCH-Method); false ==> will only save (PUT-Method)
     * @apiNote PATCH / PUT /lol-settings/v1/account/{category} [VERSION 1]
     */
    public void saveAccount(SettingCategory category, BaseSetting obj, boolean apply_directly){
        APIResponse result = null;
        if(apply_directly){
            result = patchData("lol-settings/v1/account/" + category, LCU4J.instance.getMain_gson().toJson(obj));
        }else{
            result = putData("lol-settings/v1/account/" + category, LCU4J.instance.getMain_gson().toJson(obj));
        }

        if(result.getHttpStatus() != 204){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * Save the settings in Account namespace
     * @param ppType    PreferenceType
     * @param category  Category
     * @param obj       BaseSetting to save
     * @param apply_directly    true ==> will apply directly (PATCH-Method); false ==> will only save (PUT-Method)
     * @apiNote PATCH /lol-settings/v2/account/{ppType}/{category} [VERSION 2]
     * @apiNote PUT /lol-settings/v2/account/{ppType}/{category} [VERSION 2]
     */
    public void saveAccount(String ppType, SettingCategory category, BaseSetting obj, boolean apply_directly){
        APIResponse result = null;
        if(apply_directly){
            result = patchData("lol-settings/v2/account/" + ppType + "/" + category, LCU4J.instance.getMain_gson().toJson(obj));
        }else{
            result = putData("lol-settings/v2/account/" + ppType + "/" + category, LCU4J.instance.getMain_gson().toJson(obj));
        }

        if(result.getHttpStatus() != 204){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * Returns if the reset function is used
     * @return true ==> reset function is used; false ==> reset function is not used
     * @apiNote GET /lol-settings/v1/account/didreset [VERSION 1]
     */
    public boolean didReset(){
        APIResponse result = getData("lol-settings/v1/account/didreset");

        if(result.getHttpStatus() == 200){
            return (result.getObject_string().equalsIgnoreCase("true"));
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return false;
        }
    }

    /**
     * Returns if the reset function for a specific preference type is used
     * @param ppType preference type
     * @return true ==> reset function is used; false ==> reset function is not used
     * @apiNote GET /lol-settings/v2/account/didreset/{ppType} [VERSION 2]
     */
    public boolean didReset(PPType ppType){
        APIResponse result = getData("lol-settings/v2/account/didreset/" + ppType);

        if(result.getHttpStatus() == 200){
            return (result.getObject_string().equalsIgnoreCase("true"));
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return false;
        }
    }

    /**
     * Returns settings of a specific preference type and category
     * @param ppType    preference type
     * @param category  category type
     * @return A BaseSetting-object if successfull; null if not
     * @apiNote GET /lol-settings/v2/account/{ppType}/{category} [VERSION 2]
     */
    private BaseSetting getAccount(PPType ppType, SettingCategory category){
        APIResponse result = getData("lol-settings/v2/account/" + ppType + "/" + category);

        if(result.getHttpStatus() == 200){
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(),BaseSetting.class);
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return null;
        }
    }

    /**
     * Returns settings of a specific category in a specific namespace
     * @param suburl    namespace (account / local)
     * @param category  category
     * @return A BaseSetting object if successfull; null if not
     * @apiNote GET /lol-settings/v1/account/{category} [VERSION 1]
     * @apiNote GET /lol-settings/v1/local/{category} [VERSION 1]
     */
    private BaseSetting getSetting(String suburl, SettingCategory category){
        APIResponse result = getData("lol-settings/v1/" + suburl + "/" + category);

        if(result.getHttpStatus() == 200){
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(),BaseSetting.class);
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return null;
        }
    }

    /**
     * Return settings of a specific category in account namespace
     * @param category category
     * @return A BaseSetting object if successfull; null if not
     * @apiNote GET /lol-settings/v1/account/{category} [VERSION 1]
     */
    public BaseSetting getAccount(SettingCategory category){
        return getSetting("account",category);
    }

    /**
     * Return settings of a specific category in local namespace
     * @param category category
     * @return A BaseSetting object if successfull; null if not
     * @apiNote GET /lol-settings/v1/local/{category} [VERSION 1]
     */
    public BaseSetting getLocal(SettingCategory category ){
        return getSetting("local",category);
    }


    /**
     * Save a BaseSetting object in local namespace
     * @param version   which endpoint version to use
     * @param category  category
     * @param obj       BaseSetting object
     * @apiNote PATCH /lol-settings/v1/local/{category} [VERSION 1]
     * @apiNote PATCH /lol-settings/v2/local/{category} [VERSION 2]
     */
    public void saveLocal(EndpointVersion version, SettingCategory category,BaseSetting obj){
        APIResponse result = null;
        result = patchData("lol-settings/"+ version.getValue() + "/local/" + category, LCU4J.instance.getMain_gson().toJson(obj));

        if(result.getHttpStatus() != 204){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * returns if the settings plugin is ready
     * @return true if it is ready; false if not
     * @apiNote GET /lol-settings/v2/ready [VERSION 2]
     */
    public boolean isReady(){
        APIResponse result = getData("lol-settings/v2/ready");

        if(result.getHttpStatus() == 200){
            return (result.getObject_string().equalsIgnoreCase("true"));
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return false;
        }
    }

}
