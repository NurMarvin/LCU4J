package de.eonadev.lcu4j.plugins.lolsettings;

/**
 * Abstract wrapper to access a BaseSetting object better
 */
public abstract class BaseTypeWrapper {

    /**
     * instance of BaseSetting to work with
     */
    private BaseSetting source_object = null;

    /**
     * initialize a new instance with the given BaseSetting object
     * @param src instance of BaseSetting
     */
    public  BaseTypeWrapper(BaseSetting src){
        source_object = src;
    }

    /**
     * returns the instance of BaseSetting to work with
     * @return instance of BaseSetting
     */
    public BaseSetting getSource_object() {
        return source_object;
    }

    /**
     * sets the instance of BaseSetting to work with
     * @param source_object instance of BaseSetting to set
     */
    private void setSource_object(BaseSetting source_object) {
        this.source_object = source_object;
    }
}
