package de.eonadev.lcu4j.plugins.lolsettings.typeWrapper;

import de.eonadev.lcu4j.enums.WindowSize;
import de.eonadev.lcu4j.exceptions.SettingNotFoundException;
import de.eonadev.lcu4j.plugins.lolsettings.BaseSetting;
import de.eonadev.lcu4j.plugins.lolsettings.BaseTypeWrapper;

/**
 * Wrapper for video settings
 * namespace = local
 * category = video
 */
public class VideoSettings extends BaseTypeWrapper {


    public VideoSettings(BaseSetting obj){
        super(obj);
    }

    /**
     * Sets the window size
     * @param new_size new size for the window
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void setWindowSize(WindowSize new_size) throws SettingNotFoundException{
        getSource_object().setData("ZoomScale",new_size.getValue());
    }

    /**
     * returns the window size
     * @return item of WindowSize enumeration
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public WindowSize getWindowSize() throws SettingNotFoundException{
        double ws = (double) getSource_object().getData("ZoomScale");

        if(ws == 0.75){
            return WindowSize.wr_1024_576;
        }else if (ws == 1.0){
            return WindowSize.wr_1280_720;
        }else if (ws == 1.25){
            return WindowSize.wr_1600_900;
        }else{
            return WindowSize.wr_unknown;
        }
    }


}
