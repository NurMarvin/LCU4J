package de.eonadev.lcu4j.plugins.lolsettings.typeWrapper;

import de.eonadev.lcu4j.exceptions.SettingNotFoundException;
import de.eonadev.lcu4j.plugins.lolsettings.BaseSetting;
import de.eonadev.lcu4j.plugins.lolsettings.BaseTypeWrapper;

/**
 * Wrapper for user experience
 * namespace = local
 * category = lol-user-experience
 */
public class UserExperience extends BaseTypeWrapper {

    public UserExperience(BaseSetting obj){
        super(obj);
    }


    //PART hasBeenPrompedForPotatoMode

    /**
     * returns if the user got asked to activate potato mode
     * @return true if done; false if not
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public boolean hasBeenPromptedForPotatoMode() throws SettingNotFoundException {
        return (boolean) getSource_object().getData("hasBeenPromptedForPotatoMode");
    }

    /**
     * set whether the user got asked to activate potato mode or not
     * @param value true ==> got asked; false ==> didn`t got asked
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void setHasBeenPromptedForPotatoMode(boolean value) throws SettingNotFoundException{
        getSource_object().setData("hasBeenPromptedForPotatoMode", value);
    }



    //PART lastKnownMachineSpec

    /**
     * returns the id of the last known machine specification
     * @return the id of the last known machine specification
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public int getLastKnownMachineSpec() throws SettingNotFoundException{
        return (int) getSource_object().getData("lastKnownMachineSpec");
    }

    /**
     * sets the id of the last known machine specification
     * @param value id of the machine specification
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void setLastKnownMachineSpec(int value) throws SettingNotFoundException{
        getSource_object().setData("lastKnownMachineSpec", value);
    }



    //PART potatoModeEnabled

    /**
     * returns if potato mode is enabled
     * @return true ==> potato mode is enabled; false if not
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public boolean isPotatoModeEnabled() throws SettingNotFoundException{
        return (boolean) getSource_object().getData("potatoModeEnabled");
    }

    /**
     * enables the potato mode
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void enablePotatoMode() throws SettingNotFoundException{
        getSource_object().setData("potatoModeEnabled",true);
    }

    /**
     * disables the potato mode
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void disablePotatoMode() throws SettingNotFoundException{
        getSource_object().setData("potatoModeEnabled",false);
    }


    //PART unloadUxInGameEnabled

    /**
     * returns if the LCU shall be closed if the user is in Game
     * @return  true ==> LCU shall be closed while ingame; false ==> LCU shall be opened while ingame
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public boolean isUnloadUxInGameEnabled() throws SettingNotFoundException{
        return (boolean) getSource_object().getData("unloadUxInGameEnabled");
    }

    /**
     * enabling the feature to close LCU while user is in game
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void enableUnloadUxInGame() throws SettingNotFoundException{
        getSource_object().setData("unloadUxInGameEnabled",true);
    }

    /**
     * disabling the feature to close LCU while user is in game
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void disableUnloadUxInGame() throws SettingNotFoundException{
        getSource_object().setData("unloadUxInGameEnabled",false);
    }





}
