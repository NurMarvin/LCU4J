package de.eonadev.lcu4j.plugins.lollobby;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

public class LolLobby extends BasePlugin {

    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean is_created = false;

    public LolLobby() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-lobby");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */


    /**
     * Returns whether the autofill message is displayed or not
     * @return true if displayed; false if not or an error occured
     * @apiNote GET lol-lobby/v1/autofill-displayed
     */
    public boolean isAutoFillDisplayed(){
        APIResponse result = getData("lol-lobby/v1/autofill-displayed");

        if(result.getHttpStatus() < 300){
            return (result.getObject_string().equalsIgnoreCase("true"));
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return false;
        }
    }

    /**
     * Displays the autofill message
     * @apiNote PUT lol-lobby/v1/autofill-displayed
     */
    public void displayAutoFill(){
        APIResponse result = putData("lol-lobby/v1/autofill-displayed",null);

        if(result.getHttpStatus() >= 300){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * Deletes Clash??
     * @apiNote DELETE lol-lobby/v1/clash
     */
    public void deleteClash(){
        APIResponse result = deleteData("lol-lobby/v1/clash");

        if(result.getHttpStatus() >= 300){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }


    /**
     * Post a Clash Match?
     * @param token
     * @apiNote POST lol-lobby/v1/clash
     */
    public void postClash(String token){
        APIResponse result = postData("lol-lobby/v1/clash",token);

        if(result.getHttpStatus() >= 300){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }


    /**
     * Retrieve an array of currently active custom games
     * @return array of CustomGames; null if an error occured
     * @apiNote GET lol-lobby/v1/custom-games
     */
    public LolLobbyLobbyCustomGame[] getCustomGames(){
        APIResponse result = getData("lol-lobby/v1/custom-games");

        if(result.getHttpStatus() <= 300){
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(),LolLobbyLobbyCustomGame[].class);
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return  null;
        }
    }


    /**
     * Refreshes the list of custom games
     * @apiNote POST lol-lobby/v1/custom-games/refresh
     */
    public void refreshListOfCustomGames(){
        APIResponse result = postData("lol-lobby/v1/custom-games/refresh",null);

        if(result.getHttpStatus() >= 300){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }

    /**
     * Retrieves an CustomGame object with the given ID
     * @param id id of the game
     * @return an instance of CustomGame on success; null on error
     * @apiNote GET lol-lobby/v1/custom-games/{id}
     */
    public LolLobbyLobbyCustomGame getCustomGame(int id){
        APIResponse result = getData("/lol-lobby/v1/custom-games/" + String.valueOf(id));

        if(result.getHttpStatus() < 300){
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(),LolLobbyLobbyCustomGame.class);
        }else{
            ErrorManager.getInstance().handleAPIError(result);
            return null;
        }
    }

    /**
     * Joins a specific Custom Game
     * @param id ID of the custom game
     * @apiNote POST lol-lobby/v1/custom-games/{id}/join
     */
    public void joinCustomGame(int id){
        APIResponse result = postData("/lol-lobby/v1/custom-games/" + String.valueOf(id) + "/join",null);

        if(result.getHttpStatus() >= 300){
            ErrorManager.getInstance().handleAPIError(result);
        }
    }




}
