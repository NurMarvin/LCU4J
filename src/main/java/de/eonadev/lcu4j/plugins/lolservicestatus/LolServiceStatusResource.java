package de.eonadev.lcu4j.plugins.lolservicestatus;

import de.eonadev.lcu4j.enums.Status;

/**
 * Response for lcu-status
 */
public class LolServiceStatusResource {

    /**
     * URL to the status page
     */
    private String humanReadableUrl = "";

    /**
     * Current status of the League Service
     */
    private Status status = Status.unknown;


    /**
     * Returns the URL to the status page
     * @return
     */
    public String getHumanReadableUrl() {
        return humanReadableUrl;
    }

    /**
     * Returns the current status
     * @return
     */
    public Status getStatus() {
        return status;
    }


    LolServiceStatusResource() {
    }

}
