package de.eonadev.lcu4j.plugins.lolservicestatus;

import de.eonadev.lcu4j.enums.Severity;

/**
 * Response for ticker-messages
 */
public class LolServiceStatusTickerMessage {

    /**
     * date and time of creation of this message
     */
    private String createdAt = "";

    /**
     * title of the message
     */
    private String heading = "";

    /**
     * content of the message
     */
    private String message = "";

    /**
     * priority of the message
     */
    private Severity severity = Severity.info;

    /**
     * date and time which the message was updated
     */
    private String updatedAt = "";

    /**
     * returns the date and time of creation of this message as string
     * @return
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * returns the title of this message
     * @return
     */
    public String getHeading() {
        return heading;
    }

    /**
     * returns the content of this message
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * returns priority of this message
     * @return
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * returns date and time which this message was updated
     * @return
     */
    public String getUpdatedAt() {
        return updatedAt;
    }
}
