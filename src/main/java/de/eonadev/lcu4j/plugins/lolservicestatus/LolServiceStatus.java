package de.eonadev.lcu4j.plugins.lolservicestatus;


import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

/**
 * Endpoint to plugin lol-service-status
 */
public class LolServiceStatus extends BasePlugin {

    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean is_created = false;

    public LolServiceStatus() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-service-status");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */

    /**
     * Returns the current status of League of Legends-Servers and the URL to the status page
     * @return
     */
    public LolServiceStatusResource getLCUStatus() {
        APIResponse data = getData("lol-service-status/v1/lcu-status");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolServiceStatusResource.class);
        } else {
            ErrorManager.getInstance().handleAPIError(data);
            return null;
        }
    }

    /**
     * Returns a list of ticker messages from status-server
     * @return
     */
    public LolServiceStatusTickerMessage[] getTickerMessages() {
        APIResponse data = getData("lol-service-status/v1/ticker-messages");

        if (data.getHttpStatus() == 200) {

            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolServiceStatusTickerMessage[].class);
        } else {
            ErrorManager.getInstance().handleAPIError(data);
            return null;
        }
    }


}
