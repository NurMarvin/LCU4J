package de.eonadev.lcu4j.plugins.lolspectator;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;

public class LolSpectator extends BasePlugin {
    /**
     * SINGLETON START (access shall only be over LCU4J)
     */

    private static boolean is_created = false;

    public LolSpectator() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-spectator");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */

    /**
     * Returns the Informations about acutally spectating game
     *
     * @return LolSpectatorSpectateGameInfo.class
     */
    public LolSpectatorSpectateGameInfo getLolSpectatorSpectateGameInfo() {
        APIResponse data = getData("lol-spectator/v1/spectate");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolSpectatorSpectateGameInfo.class);
        } else {
            return null;
        }
    }

    /**
     * TODO @Blood get oder direkt den Launch Befehl hier einfügen?
     * Returns a list of ticker messages from status-server
     *
     * @return LolSpectatorSpectateLaunch.class
     */
    public LolSpectatorSpectateLaunch getLolSpectatorSpectateLaunch() {
        APIResponse data = getData("lol-spectator/v1/spectate/launch");

        if (data.getHttpStatus() == 200) {

            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolSpectatorSpectateLaunch.class);
        } else {
            return null;
        }
    }

    /**
     * Returns
     *
     * @return LolSpectatorSummonerOrTeamAvailabilty.class
     */
    public LolSpectatorSummonerOrTeamAvailabilty getLolSpectatorSummonerOrTeamAvailabilty() {
        APIResponse data = getData("lol-spectator/v1/buddy/spectate");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolSpectatorSummonerOrTeamAvailabilty.class);
        } else {
            return null;
        }
    }
}
