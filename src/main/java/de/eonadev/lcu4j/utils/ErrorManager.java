package de.eonadev.lcu4j.utils;

import de.eonadev.lcu4j.exceptions.websocket.*;

import java.util.ArrayList;


/**
 * Contain all errors which are thrown from LCU4J
 */
public class ErrorManager {

    /**
     * SINGLETON START
     */

    private static ErrorManager instance = null;

    public static ErrorManager getInstance(){
        if(instance == null){
            instance = new ErrorManager();
        }
        return instance;
    }

    private ErrorManager(){

    }

    /**
     * SINGLETON END
     */

    /**
     * List of callbacks, which shall run, if an error occurs
     */
    private ArrayList<ErrorCallback> callbacks = new ArrayList<>();

    /**
     * Indicates whether the errors shall be visible in the console
     */
    private boolean log_to_console = false;

    /**
     * Executes the callbacks
     * @param e
     */
    private void runCallbacks(Exception e){
        for(ErrorCallback ec:callbacks){
            ec.onErrorReceived(e);
        }
    }

    /**
     * Add a exception to manage
     * @param e
     */
    public void raiseException(Exception e){
        if(log_to_console){
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                runCallbacks(e);
            }
        });
    }


    /**
     * add a new Callback
     * @param callback
     */
    public void addCallback(ErrorCallback callback){
        callbacks.add(callback);
    }

    /**
     * Remove a callback
     * @param callback
     */
    public void removeCallback(ErrorCallback callback){
        callbacks.remove(callback);
    }


    /**
     * Processing failed API Requests
     * @param response failed APIResponse
     */
    public void handleAPIError(APIResponse response){
        switch(response.getHttpStatus()){
            case 404:
                this.raiseException(new NotFoundException(response));
            case 403:
                this.raiseException(new ForbiddenException(response));
            case 400:
                this.raiseException(new BadRequestException(response));
            case 401:
                this.raiseException(new UnauthorizedException(response));
            case 503:
                this.raiseException(new ServiceUnavailableException(response));
            case 500:
                this.raiseException(new NotFoundException(response));
            case 415:
                this.raiseException(new UnsupportedMediaTypeException(response));
        }
    }

    public boolean isLog_to_console() {
        return log_to_console;
    }

    public void setLog_to_console(boolean log_to_console) {
        this.log_to_console = log_to_console;
    }
}
