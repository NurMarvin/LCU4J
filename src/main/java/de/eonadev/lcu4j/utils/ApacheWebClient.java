package de.eonadev.lcu4j.utils;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.websocket.BadPortException;
import org.apache.http.Header;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URI;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * Connect to LCU api with the apache http client
 */
public class ApacheWebClient {

    /**
     * SINGLETON START
     */

    public static ApacheWebClient instance = new ApacheWebClient();

    private ApacheWebClient(){

    }

    /**
     * SINGLETON END
     */


    /**
     * tcp port the LCU api listens to
     */
    private int port = -1;

    /**
     * authentication key for webclient
     */
    private String auth_key = "";

    /**
     * base connection
     */
    private CloseableHttpClient client = null;


    private HttpGet default_get = null;


    private HttpPost default_post = null;


    private HttpPatch default_patch = null;


    private HttpPut default_put = null;


    private HttpDelete default_delete = null;


    /**
     * returns the port of the webserver
     * @return the port of the webserver or -1 if not set
     */
    public int getPort() {
        return port;
    }

    /**
     * set the port which the client shall connect to
     * @param port  port number
     * @throws BadPortException if the port is not in range between 0 and 65535
     */
    private void setPort(int port)throws BadPortException {
        if (port > 0 && port <= 65535) {
            this.port = port;
        } else {
            throw new BadPortException(port);
        }
    }

    /**
     * returns the auth key for the webclient
     * @return authentication key as string or an empty string if not set
     */
    public String getAuth_key() {
        return auth_key;
    }

    /**
     * sets the auth key for the webclient
     * @param auth_key new auth key
     */
    private void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    /**
     * sets the essential properties for connecting to
     * @param port
     * @param auth_key
     */
    public void setConnectionData(int port, String auth_key){
        try {
            setPort(port);
            setAuth_key(auth_key);
            prepareMethods();
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
    }


    /**
     * prepares the client and the requests
     */
    public void openClient(){
        try{
            SSLContext sslContext = SSLContext.getInstance("SSL");

            // set up a TrustManager that trusts everything
            sslContext.init(null, new TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs,
                                               String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                }
            } }, new SecureRandom());


            client = HttpClients.custom().setSSLContext(sslContext).build();
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
    }

    /**
     * prepare the methods
     */
    private void prepareMethods(){
        try{
            //Standard Headers
            Header default_header[] = null;

            //GET-Method
            default_get = new HttpGet();
            default_get.addHeader("Content-Type","application/json");
            default_get.addHeader("Accept","application/json");
            default_get.addHeader("Authorization", "Basic " + getAuth_key());


            default_header = default_get.getAllHeaders();


            //POST-Method
            default_post = new HttpPost();
            default_post.setHeaders(default_header);


            //PATCH-Method
            default_patch = new HttpPatch();
            default_patch.setHeaders(default_header);


            //PUT-Method
            default_put = new HttpPut();
            default_put.setHeaders(default_header);

            //DELETE-Method
            default_delete = new HttpDelete();
            default_delete.setHeaders(default_header);


        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
    }


    /**
     * Closes the client
     */
    public void closeClient(){
        if(client != null){
            try{
                client.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    /**
     * Executes the request
     * @param request   Request to be made
     * @return  an instance of APIResponse
     */
    private APIResponse executeRequest(HttpRequestBase request, APIRequest apiRequest){
        APIResponse result = new APIResponse(apiRequest);

        try{
            //Executes request and get response
            CloseableHttpResponse response = client.execute(request);

            result.setHttpStatus(response.getStatusLine().getStatusCode());
            String response_string = EntityUtils.toString(response.getEntity());
            if(result.getHttpStatus() >= 200 && result.getHttpStatus() < 300){
                result.setObject_string(response_string);
            }else{
                result = LCU4J.instance.getMain_gson().fromJson(response_string,APIResponse.class);
            }

        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }

        return result;
    }

    /**
     * Executes a GET-Request
     * @param endpoint_method   Name of the method (with path parameters)
     * @return an instance of APIResponse or null if something bad happened
     */
    public APIResponse executeGET(String endpoint_method){
        try{
            HttpGet current_request = (HttpGet) default_get.clone();

            current_request.setURI(URI.create("https://127.0.0.1:" + String.valueOf(getPort()) + "/" + endpoint_method));

            return executeRequest(current_request,APIRequest.createRequest(endpoint_method,"GET",""));

        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
        return null;
    }

    /**
     * Executes a POST-Request of the given endpoint and the json_data
     * @param endpoint_method method
     * @param json_data POST-Data as JSON
     * @return  an instance of APIResponse or null if something bad happened
     */
    public APIResponse executePOST(String endpoint_method, String json_data){
        try{
            HttpPost current_request = (HttpPost) default_post.clone();
            current_request.setURI(URI.create("https://127.0.0.1:" + String.valueOf(getPort()) + "/" + endpoint_method));

            if(json_data != null){
                current_request.setEntity(new ByteArrayEntity(json_data.getBytes()));
            }

            return executeRequest(current_request,APIRequest.createRequest(endpoint_method,"POST",json_data));
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
        return  null;
    }

    /**
     * Executes a PATCH-Request
     * @param endpoint_method method
     * @param json_data PATCH-Data as JSON
     * @return  an instance of APIResponse or null if something bad happened
     */
    public APIResponse executePATCH(String endpoint_method, String json_data){
        try{
            HttpPatch current_request = (HttpPatch) default_patch.clone();
            current_request.setURI(URI.create("https://127.0.0.1:" + String.valueOf(getPort()) + "/" + endpoint_method));

            if(json_data != null){
                ByteArrayEntity ba = new ByteArrayEntity(json_data.getBytes(), ContentType.APPLICATION_JSON);
                current_request.setEntity(ba);
            }

            return executeRequest(current_request, APIRequest.createRequest(endpoint_method,"PATCH",json_data));
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
        return  null;
    }

    /**
     * Executes a PUT-Request
     * @param endpoint_method method to execute
     * @param json_data PUT-data as JSON
     * @return an instance of APIResponse or null if something bad happened
     */
    public APIResponse executePUT(String endpoint_method, String json_data){
        try{
            HttpPut current_request = (HttpPut) default_put.clone();
            current_request.setURI(URI.create("https://127.0.0.1:" + String.valueOf(getPort()) + "/" + endpoint_method));

            if(json_data != null){
                current_request.setEntity(new ByteArrayEntity(json_data.getBytes()));
            }

            return executeRequest(current_request,APIRequest.createRequest(endpoint_method,"PUT",json_data));
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
        return  null;
    }

    /**
     * Executes a DELETE-Request
     * @param endpoint_method   method to execute
     * @return  an instance of APIResponse or null if something happened
     */
    public APIResponse executeDELETE(String endpoint_method){
        try{
            HttpDelete current_request = (HttpDelete) default_delete.clone();
            current_request.setURI(URI.create("https://127.0.0.1:" + String.valueOf(getPort()) + "/" + endpoint_method));


            return executeRequest(current_request,APIRequest.createRequest(endpoint_method,"DELETE",""));
        }catch (Exception e){
            ErrorManager.getInstance().raiseException(e);
        }
        return  null;
    }





}
