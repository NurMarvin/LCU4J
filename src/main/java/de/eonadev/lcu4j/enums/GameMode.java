package de.eonadev.lcu4j.enums;

public enum GameMode {

    unknown(""),
    classic("CLASSIC"),
    gamemodex("GAMEMODEX"),
    aram("ARAM");

    private String x_gamemode = "";

    GameMode(String s){
        x_gamemode = s;
    }


    public String getGamemode() {
        return x_gamemode;
    }
}
