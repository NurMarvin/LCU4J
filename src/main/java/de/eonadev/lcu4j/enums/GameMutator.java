package de.eonadev.lcu4j.enums;

public enum GameMutator {
    BLIND(1),
    DRAFT(2),
    ALL_RANDOM(5),
    TOURNEY_DRAFT(6);

    private int gameMutatorID = 0;

    GameMutator(int integer) {
        gameMutatorID = integer;
    }

    public int getValue() {
        return gameMutatorID;
    }
}
