package de.eonadev.lcu4j.enums;

public enum WindowSize {

    wr_unknown(0.8),
    wr_1280_720(1.0),
    wr_1024_576(0.8),
    wr_1600_900(1.25);

    private double x_windowSize = 0;

    WindowSize(Double e){
        x_windowSize = e;
    }

    public  double getValue(){
        return x_windowSize;
    }


}
