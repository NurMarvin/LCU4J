package de.eonadev.lcu4j.enums;

public enum SettingCategory  {

    unknown ("unknown"),
    video ("video"),
    lol_general("lol-general"),
    lol_notifications("lol-notifications"),
    lol_user_experience("lol-user-experience");


    private String x_settingcategory = "";

    SettingCategory(String s){
        x_settingcategory = s;
    }


    public String getValue(){
        return x_settingcategory;
    }


}
