package de.eonadev.lcu4j.enums;

public enum InventoryType {

    unknown("unknown"),
    EMOTE("EMOTE"),
    TOURNAMENT_FLAG("TOURNAMENT_FLAG"),
    TOURNAMENT_TROPHY("TOURNAMENT_TROPHY");


    private String x_inventoryType = "";

    InventoryType(String s) {
        x_inventoryType = s;
    }

    public String getValue() {
        return x_inventoryType;
    }
}
