package de.eonadev.lcu4j.enums;

public enum EndpointVersion {

    version1("v1"),
    version2("v2");

    private String this_version = "";

    EndpointVersion(String s){
        this.this_version = s;
    }


    public String getValue(){
        return this_version;
    }


}
