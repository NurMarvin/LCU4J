package de.eonadev.lcu4j.enums;

public enum Ranks {

    unknown("UNKNOWN"),
    unranked("UNRANKED"),
    iron("IRON"),
    bronze("BRONZE"),
    silver("SILVER"),
    gold("GOLD"),
    platin("PLATIN"),
    diamant("DIAMANT"),
    master("MASTER"),
    grandmaster("GRANDMASTER"),
    challenger("CHALLENGER");

    private String x_rank = "";

    Ranks(String s) {
        x_rank = s;
    }


    public String getValue(){
        return x_rank;
    }

}
