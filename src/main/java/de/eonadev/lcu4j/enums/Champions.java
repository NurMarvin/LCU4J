package de.eonadev.lcu4j.enums;

public enum Champions {
    Aatrox("Aatrox"),
    Ahri("Ahri"),
    Akali("Akali"),
    Alistar("Alistar"),
    Amumu("Amumu"),
    Anivia("Anivia"),
    Annie("Annie"),
    Ashe("Ashe"),
    AurelionSol("Aurelion Sol"),
    Azir("Azir"),


    Bard("Bard"),
    Blitzcrank("Blitzcrank"),
    Brand("Brand"),
    Braum("Braum"),


    Caitlyn("Caitlyn"),
    Camille("Camille"),
    Cassiopeia("Cassiopeia"),
    ChoGath("Cho'Gath"),
    Corki("Corki"),

    Darius("Darius"),
    Diana("Diana"),
    DrMundo("Dr. Mundo"),
    Draven("Draven"),

    Ekko("Ekko"),
    Elise("Elise"),
    Evelynn("Evelynn"),
    Ezreal("Ezreal"),

    Fiddlesticks("Fiddlesticks"),
    Fiora("Fiora"),
    Fizz("Fizz"),

    Galio("Galio"),
    Gangplank("Gangplank"),
    Garen("Garen"),
    Gnar("Gnar"),
    Gragas("Gragas"),
    Graves("Graves"),

    Hecarium("Hecarium"),
    Heimerdinger("Heimerdinger"),

    Illaoi("Illaoi"),
    Irelia("Irelia"),
    Ivern("Ivern"),

    Janna("Janna"),
    JaravanIV("Jaravan IV"),
    Jax("Jax"),
    Jayce("Jhin"),
    Jinx("Jinx"),

    KaiSa("Kai'Sa"),
    Kalista("Kalista"),
    Karma("Karma"),
    Karthus("Karthus"),
    Kassadin("Kassadin"),
    Katarina("Katarina"),
    Kayle("Kayle"),
    Kayn("Kayn"),
    Kennen("Kennen"),
    KhaZix("Kha'Zix"),
    Kindred("Kindred"),
    Kled("Kled"),
    KogMaw("Kog'Maw"),

    LeBlanc("LeBlanc"),
    LeeSin("Lee Sin"),
    Leona("Leona"),
    Lissandra("Lissandra"),
    Lucian("Lucian"),
    Lulu("Lulu"),
    Lux("Lux"),

    Malphite("Malphite"),
    Malzahar("Malzahar"),
    Maokai("Maokai"),
    MasterYi("MasterYi"),
    MissFortune("Miss Fortune"),
    Mordekaiser("Mordekaise"),
    Morgana("Morgana"),

    Nami("Nami"),
    Nasus("Nasus"),
    Nautilus("Nautilus"),
    Neeko("Neeko"),
    Nidalee("Nidalee"),
    Nocturne("Nocturne"),
    Nunu("Nunu & Willump"),

    Olaf("Olaf"),
    Orianna("Orianna"),
    Ornn("Ornn"),

    Pantheon("Pantheon"),
    Poppy("Poppy"),
    Pyke("Pyke"),

    Quinn("Quinn"),

    Rakan("Rakan"),
    Rammus("Rammus"),
    RekSai("Rek'Sai"),
    Renekton("Renekton"),
    Rengar("Rengar"),
    Riven("Riven"),
    Rumble("Rumble"),
    Ryze("Ryze"),

    Sejuani("Sejuani"),
    Shaco("Shaco"),
    Shen("Shen"),
    Shyvana("Shyvana"),
    Singed("Singed"),
    Sion("Sion"),
    Sivir("Sivir"),
    Skarner("Skarner"),
    Sona("Sona"),
    Soraka("Soraka"),
    Swain("Swain"),
    Syndra("Syndra"),

    TahmKench("Tahm Kench"),
    Taliyah("Taliyah"),
    Talon("Talon"),
    Taric("Taric"),
    Teemo("Teemo"),
    Thresh("Thresh"),
    Tristana("Tristana"),
    Trundle("Trundle"),
    Tryndamere("Tryndamere"),
    TwistedFate("Twisted Fate"),
    Twitch("Twitch"),

    Udyr("Udyr"),
    Urgot("Urgot"),

    Varus("Varus"),
    Vayne("Vayne"),
    Veigar("Veigar"),
    VelKoz("Vel'Koz"),
    Vi("Vi"),
    Viktor("Viktor"),
    Vladimir("Vladimir"),
    Volibear("Volibear"),

    Warwick("Warwick"),
    Wukong("Wukong"),

    Xayah("Xayah"),
    Xerath("Xerath"),
    XinZhao("XinZhao"),

    Yasuo("Yasuo"),
    Yorick("Yorik"),

    Zac("Zac"),
    Zed("Zed"),
    Ziggs("Ziggs"),
    Zilean("Zilean"),
    Zoe("Zoe"),
    Zyra("Zyra");

    private String champ = "";

    Champions(String s) {
        champ = s;
    }

    public String getValue() {
        return champ;
    }
}
