package de.eonadev.lcu4j.enums;

public enum SpectatorPolicy {

    not_allowed("NotAllowed"),
    lobby_allowed("LobbyAllowed"),
    friends_allowed("FriendsAllowed"),
    all_allowed("AllAllowed");


    private String x_spectatorPolicy = "";

    SpectatorPolicy(String s) {
        x_spectatorPolicy = s;
    }


    public String getValue(){
        return x_spectatorPolicy;
    }

}
