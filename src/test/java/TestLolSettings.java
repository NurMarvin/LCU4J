import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.enums.EndpointVersion;
import de.eonadev.lcu4j.enums.LCUEndpoint;
import de.eonadev.lcu4j.enums.SettingCategory;
import de.eonadev.lcu4j.enums.WindowSize;
import de.eonadev.lcu4j.plugins.lolsettings.BaseSetting;
import de.eonadev.lcu4j.plugins.lolsettings.LolSettings;
import de.eonadev.lcu4j.plugins.lolsettings.typeWrapper.VideoSettings;


public class TestLolSettings {


    public static void main(String args[]){
        LCU4J api = new LCU4J();

        LolSettings lolSettings = (LolSettings) api.getEndpoint(LCUEndpoint.lol_settings);


        BaseSetting setting = lolSettings.getLocal(SettingCategory.video);

        VideoSettings videoSettings = new VideoSettings(setting);


        try{
            //Return current window size
            System.out.println(videoSettings.getWindowSize().name());


            //Set current window size
            videoSettings.setWindowSize(WindowSize.wr_1600_900);

            //Save and apply settings
            lolSettings.saveLocal(EndpointVersion.version1,SettingCategory.video,setting);


        }catch (Exception e){
            e.printStackTrace();
        }





    }

}
