import de.eonadev.lcu4j.LCU4J;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class LCUTest {

    static LCU4J api = null;


    @BeforeAll
    @DisplayName("Testing initializing of API")
    public static void testAPIInitialization(){
        api = new LCU4J();
        assertNotEquals(null,api,"API not initialized");
    }


    @Test
    @DisplayName("Testing whether the webclient for API calls is initiated")
    public void testClient(){
        assertNotEquals(null, api.getClient(), "Client is initiated");
    }


}
