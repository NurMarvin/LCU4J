import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.enums.LCUEndpoint;
import de.eonadev.lcu4j.enums.Status;
import de.eonadev.lcu4j.plugins.lolservicestatus.LolServiceStatus;
import de.eonadev.lcu4j.plugins.lolservicestatus.LolServiceStatusResource;
import de.eonadev.lcu4j.plugins.lolservicestatus.LolServiceStatusTickerMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

/**
 * LoLServiceTestStatus
 */
public class TestLoLServiceStatus {

    private static LCU4J api = null;

    @BeforeAll
    @DisplayName("Testing initializing of API")
    public static void testAPIInitialization(){
        api = new LCU4J();
        assertNotEquals(null,api,"API not initialized");
    }

    @Test
    @DisplayName("Testing whether the endpoint is in LCU4J activated")
    public void testLoLServiceStatus(){
        LolServiceStatus o = ((LolServiceStatus) api.getEndpoint(LCUEndpoint.lol_service_status));
        assertNotNull(o,"LolServiceStatus not in API");
    }

    @Test
    @DisplayName("Testing if we can get the current status")
    public void testStatus(){
        LolServiceStatus o = ((LolServiceStatus) api.getEndpoint(LCUEndpoint.lol_service_status));
        LolServiceStatusResource lss = o.getLCUStatus();

        assertNotEquals(Status.unknown,lss.getStatus(),"Status is unknown");
        assertNotEquals("",lss.getHumanReadableUrl(),"Status page not set");
    }


    @Test
    @DisplayName("Testing if we can get the news ticker")
    public void testTicker(){
        LolServiceStatus o = ((LolServiceStatus) api.getEndpoint(LCUEndpoint.lol_service_status));
        LolServiceStatusTickerMessage[] tickerMessages = o.getTickerMessages();

        assertNotNull(tickerMessages,"Can`t get any ticker messages");
        assertNotEquals(0,tickerMessages.length,"No ticker messages found!");
    }

}
