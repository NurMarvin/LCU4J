import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.enums.LCUEndpoint;
import de.eonadev.lcu4j.plugins.lollobby.LolLobby;
import de.eonadev.lcu4j.plugins.lollobby.LolLobbyLobbyCustomGame;
import de.eonadev.lcu4j.utils.ErrorManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestLolLobby {

    private static LCU4J api = null;
    private static LolLobby lobby = null;


    @Test
    @BeforeAll
    public static void initAPI(){
        //ErrorManager.getInstance().setLog_to_console(true);
        api = new LCU4J();
        lobby = (LolLobby) api.getEndpoint(LCUEndpoint.lol_lobby);
    }


    @Test
    @DisplayName("Test if we can get a list of custom games!")
    public void testGetListOfCustomGames(){
        Assertions.assertNotEquals(0,lobby.getCustomGames().length);
    }

    @Test
    @DisplayName("Check if we can get some informations of a custom game")
    public void testGetCustomGameInformations(){
        Assertions.assertDoesNotThrow(() -> {
            LolLobbyLobbyCustomGame list[] = lobby.getCustomGames();

            if(list.length > 0 ){
                System.out.println("LobbyName: " + list[0].getLobbyName());
                System.out.println("Owner: " + list[0].getOwnerSummonerName());
                System.out.println("GameType: " + list[0].getGameType());
                System.out.println("FilledPlayerSlots: " + list[0].getFilledPlayerSlots());
                System.out.println("MaxPlayerSlots: " + list[0].getMaxPlayerSlots());
                System.out.println("FilledSpectatorSlots: " + list[0].getFilledSpectatorSlots());
                System.out.println("MaxSpectatorSlots: " + list[0].getMaxSpectatorSlots());
                System.out.println("SpectatorPolicy: " + list[0].getSpectatorPolicy());
                System.out.println("ID: " + list[0].getId());
                System.out.println("MapID: " + list[0].getMapID());
                System.out.println("GameType: " + list[0].getGameType());
                System.out.println("Password: " + list[0].hasPassword());
                System.out.println("PassBackUrl: " + list[0].getPassbackURL());

                System.out.println("========================================");
            }
        },"Something went wrong");
    }
}
