import de.eonadev.lcu4j.utils.ErrorManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestErrorManager {


    @Test
    @BeforeAll
    @DisplayName("Trying to get the instance of ErrorManager")
    public static void testErrorManager1(){
        ErrorManager e = ErrorManager.getInstance();
        assertNotEquals(null,e,"ErrorManage could not bei initialized");
    }


    @Test
    @DisplayName("Testing if callback works!")
    public void testCallback(){
        assertDoesNotThrow(() ->{
            ErrorManager.getInstance().setLog_to_console(false);
            ErrorManager.getInstance().raiseException(new Exception("I`m a test exception"));
        },"Error while testing if callback works");
    }

}
